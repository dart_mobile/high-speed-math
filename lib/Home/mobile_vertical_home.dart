import 'package:flutter/material.dart';

import '../Game.dart';

class MobileHomeVertical extends StatefulWidget {
  const MobileHomeVertical({Key? key}) : super(key: key);

  @override
  State<MobileHomeVertical> createState() => _MobileHomeVerticalState();
}

class _MobileHomeVerticalState extends State<MobileHomeVertical> {
  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return Container(
      color: Colors.lightBlue.shade900,
      child: Column(
        children: [
          const Expanded(
            flex: 1,
            child: Center(
              child: Text(""),
            ),
          ),
          Expanded(
            flex: 1,
            child: Center(
              child: Text("MATH",
                  style: TextStyle(
                      fontSize: height * 0.1,
                      fontWeight: FontWeight.bold,
                      color: Colors.white)),
            ),
          ),
          Expanded(
            flex: 1,
            // child: Center(
            child: Text("SPEED",
                style: TextStyle(
                    fontSize: height * 0.1,
                    fontWeight: FontWeight.bold,
                    color: Colors.white)),
            // ),
          ),
          Image(
            height: height * 0.35,
            image: const NetworkImage(
                'https://scontent.fbkk31-1.fna.fbcdn.net/v/t1.15752-9/333981080_905446673908533_3188371953273655919_n.png?_nc_cat=107&ccb=1-7&_nc_sid=ae9488&_nc_eui2=AeHTyfGOkJstNA15-9S4CpMsZ2asng4Jr3FnZqyeDgmvcayU3BO9gpUU97wSxcO52jTDj3fhKOO-lQgPJlf0eEDE&_nc_ohc=a8vfZE9MP6YAX8fK9XT&_nc_ht=scontent.fbkk31-1.fna&oh=03_AdSVQzbqKJWfM0hi8ZEaeeXjKM5Vi9a6n0Zz5soIQKBUng&oe=6460CA71'),
          ),
          Expanded(
            flex: 1,
            child: Center(
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.lightBlue.shade700,
                    fixedSize: Size(width * 0.5, width * 0.17),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10))),
                child: Text(
                  'Start',
                  style: TextStyle(fontSize: height * 0.04),
                ),
                onPressed: () {
                  _navigateToNextScreen(context);
                },
              ),
            ),
          ),
          const Expanded(
            flex: 1,
            child: Center(
              child: Text(""),
            ),
          ),
        ],
      ),
    );
  }

  void _navigateToNextScreen(BuildContext context) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => MainPage(),
      ),
    );
  }
}
