import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/countdown_timer_controller.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'package:high_speed_math/Numpad.dart';
import 'package:high_speed_math/Question.dart';
import 'package:high_speed_math/Text.dart';

import '../Answer.dart';
import '../AppBarMath.dart';
import 'AlertDialog/Incorrect.dart';
import 'AlertDialog/Incorrect_horizontal.dart';
import 'MainGame/result_score.dart';

class GameUiDesktop extends StatefulWidget {
  final ValueChanged incrementCounter;
  final ValueChanged randomNumber;
  final int firstNumber;
  final int secondNumber;
  final int score;

  const GameUiDesktop(
      {Key? key,
      required this.score,
      required this.incrementCounter,
      required this.randomNumber,
      required this.firstNumber,
      required this.secondNumber})
      : super(key: key);

  @override
  State<GameUiDesktop> createState() => _GameUiDesktopState();
}

class _GameUiDesktopState extends State<GameUiDesktop> {
  late CountdownTimerController controller;
  int endTime = DateTime.now().millisecondsSinceEpoch + 30000;

  final TextEditingController ansController = TextEditingController();

  @override
  void initState() {
    super.initState();
    controller = CountdownTimerController(endTime: endTime);
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    ansController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;

    final appbarHeight = screenWidth * 0.04;
    final appbarWidth = 1;

    final textFillWidth = screenWidth * 0.4;
    final textFillHeight = screenHeight * 0.085;
    final fontSize = screenWidth * 0.022;

    return Scaffold(
      backgroundColor: Colors.lightBlue[900],
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.lightBlue[700],
        toolbarHeight: appbarHeight,
        title: CountdownTimer(
          textStyle: TextStyle(fontSize: appbarHeight * 0.5),
          endTime: endTime,
          onEnd: () {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => result_Score(
                      score: widget.score,
                    )));
          },
        ),
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios,
            size: appbarHeight * 0.5,
          ),
          //replace with our own icon data.
        ),
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 20, top: appbarHeight * 0.2),
            child: Text(
              '${widget.score}',
              style: const TextStyle(
                  color: Color.fromARGB(255, 255, 255, 255), fontSize: 25),
            ),
          ),
        ],
      ),
      body: Column(children: <Widget>[
        const Expanded(
          flex: 1,
          child: Text(""),
        ),
        Expanded(
          flex: 2,
          child: Center(
            child: Text(
              "${widget.firstNumber} + ${widget.secondNumber} = ?",
              style: const TextStyle(color: Colors.white, fontSize: 100),
            ),
          ),
        ),
        Expanded(
          flex: 1,
          child: Center(
            child: Padding(
              padding: EdgeInsets.only(left: 30, right: 30),
              child: Text(
                "Input your answers",
                style: TextStyle(
                    fontSize: fontSize,
                    fontWeight: FontWeight.w400,
                    color: Colors.white),
              ),
            ),
          ),
        ),
        Expanded(
          flex: 1,
          child: Center(
            child: Padding(
              padding: const EdgeInsets.only(left: 30, right: 30),
              child: Container(
                width: textFillWidth,
                height: textFillHeight,
                decoration: BoxDecoration(
                  color: Colors.lightBlue.shade700,
                  borderRadius: BorderRadius.circular(15),
                ),
                child: TextField(
                  controller: ansController,
                  decoration: const InputDecoration(
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.all(12.5),
                    alignLabelWithHint: true,
                  ),
                  style: TextStyle(fontSize: fontSize),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 50.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 20, right: 20),
                    child: Container(
                      width: 170,
                      height: 60,
                      child: ElevatedButton(
                        style: ButtonStyle(
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                          ),
                          backgroundColor: MaterialStateProperty.all<Color>(
                              Colors.red.shade400),
                        ),
                        child: const Text(
                          'CLEAR',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 24,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        onPressed: () {
                          _clear();
                        },
                      ),
                    ),
                  ),
                ],
              ),
              Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 20, right: 20),
                    child: SizedBox(
                      width: 170,
                      height: 60,
                      child: ElevatedButton(
                        style: ButtonStyle(
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                          ),
                          backgroundColor: MaterialStateProperty.all<Color>(
                              Colors.green.shade400),
                        ),
                        child: const Text(
                          'SUMBIT',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 24,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        onPressed: () {
                          _Submit();
                        },
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
        const Expanded(
          flex: 1,
          child: Text(""),
        ),
      ]),
    );
  }

  void _Submit() {
    var userInput = ansController.text;
    var correctAnswer = widget.firstNumber + widget.secondNumber;
    print("${userInput} : ${correctAnswer}");
    if (userInput == correctAnswer.toString()) {
      //correct
      _onPressed();
    } else {
      // incorrect
      userInput = "";

      showDialog(
        context: context,
        builder: (context) {
          return Dialog_Horizontal(onTap: backQuestion);
        },
      );
    }
  }

  void _onPressed() {
    setState(() {
      widget.incrementCounter(null);
      widget.randomNumber(null);
      widget.score;
      ansController.text = "";
    });
  }

  void _clear() {
    ansController.text = '';
  }

  void backQuestion() {
    Navigator.of(context).pop();
  }
}
