import 'package:flutter/material.dart';

class ResponsiveLayout extends StatelessWidget {
  final Widget mobileBodyVertical;
  final Widget mobileBodyHorizontal;
  final Widget tabletBodyVertical;
  final Widget tabletBodyHorizontal;
  final Widget desktopBody;

  ResponsiveLayout(
      {required this.mobileBodyVertical,
      required this.mobileBodyHorizontal,
      required this.tabletBodyVertical,
      required this.tabletBodyHorizontal,
      required this.desktopBody});

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        //mobile
        if ((constraints.maxWidth >= 360 && constraints.maxWidth <= 481) &&
            (constraints.maxHeight >= 640 && constraints.maxHeight <= 960)) {
          return mobileBodyVertical; //<---------vertical
        } else if ((constraints.maxWidth >= 640 &&
                constraints.maxWidth <= 960) &&
            (constraints.maxHeight >= 360 && constraints.maxHeight <= 481)) {
          return mobileBodyHorizontal; //<---------horizontal
        }

        //Tablet
        else if ((constraints.maxWidth >= 800 &&
                constraints.maxWidth <= 1280) &&
            (constraints.maxHeight >= 1280 && constraints.maxHeight <= 1880) &&
            !((constraints.maxWidth >= 1000 && constraints.maxWidth <= 1290) &&
                (constraints.maxHeight >= 1500 &&
                    constraints.maxHeight <= 1700))) {
          return tabletBodyVertical; //<---------vertical


        } else if ((constraints.maxWidth >= 1080 &&
                constraints.maxWidth <= 1880) &&
            (constraints.maxHeight >= 800 && constraints.maxHeight <= 1280) &&
            !((constraints.maxWidth == 1800 && constraints.maxHeight == 970)) ) {
          return tabletBodyHorizontal; //<---------horizontal
        }
        //Desktop
        else if ((constraints.maxWidth >= 1620 &&
                constraints.maxWidth <= 1800) &&
            (constraints.maxHeight >= 740 && constraints.maxHeight <= 970)) {
          return desktopBody;
        } else {
          return tabletBodyVertical;
        }
      },
    );
  }
}
