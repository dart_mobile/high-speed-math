import 'package:flutter/material.dart';
import 'package:high_speed_math/Store/appmodel.dart';
import 'package:provider/provider.dart';
import 'HomPage.dart';
import 'package:device_preview/device_preview.dart';

void main() {
  runApp(
    DevicePreview(
      enabled: true,
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        home: Provider(create: (_) => AppModel(), child: const HomePage()),
      ),
    ),
  );
}
