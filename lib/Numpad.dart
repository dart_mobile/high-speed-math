import 'dart:math';

import 'package:flutter/material.dart';

import 'AlertDialog/Incorrect.dart';

String showInTextFill = "Answer";

int firstNumber = RandomNumber(1);
int secondNumber = RandomNumber(1);

class BuildBtn extends StatefulWidget {
  final double fontSize;
  final double aspectRatio;
  final double paddingSize;
  final double fontAnsSize;
  final textFillWidth;
  final textFillHeight;

  const BuildBtn({
    Key? key,
    required this.fontSize,
    required this.aspectRatio,
    required this.paddingSize,
    required this.textFillWidth,
    required this.textFillHeight,
    required this.fontAnsSize,

  }) : super(key: key);

  @override
  _BuildBtnState createState() => _BuildBtnState();
}

class _BuildBtnState extends State<BuildBtn> {
  final showText = "";

  var btnColor = Colors.white70;
  var textColor = Colors.white;
  var numPadIndex;

  final List<String> numPad = [
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    'Del',
    '0',
    'Ans'
  ];

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;

    final fontSize = screenHeight * 0.02;

    final Function() onReload;
    //number pad
    return Column(
      children: [
        Expanded(
          flex: 2,
          child: Center(
            child: Padding(
              padding: const EdgeInsets.only(left: 0, right: 0),
              child: Container(
                width: widget.textFillWidth,
                height: widget.textFillHeight,
                decoration: BoxDecoration(
                  color: Colors.lightBlue.shade700,
                  borderRadius: BorderRadius.circular(15),
                ),
                child: Center(
                  child: Text(
                    showInTextFill,
                    style: TextStyle(
                        fontSize: 15, color: Colors.grey.shade400),
                  ),
                ),
              ),
            ),
          ),
        ),
        Expanded(
          flex: 4,
          child: GridView.builder(
            itemCount: numPad.length,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 3,
              childAspectRatio: widget.aspectRatio,
            ),
            itemBuilder: (context, index) {
              numPadIndex = numPad[index];
              if (numPadIndex == "Del") {
                btnColor = Colors.red.shade400;
                textColor = Colors.white;
              } else if (numPadIndex == "Ans") {
                btnColor = Colors.green.shade400;
                textColor = Colors.white;
              } else {
                btnColor = Colors.white70;
                textColor = Colors.black;
              }
              return Padding(
                padding: EdgeInsets.all(widget.paddingSize),
                child: GestureDetector(
                  onTap: () => buttonTaped(numPad[index]),
                  child: Container(
                    decoration: BoxDecoration(
                      color: btnColor,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Center(
                      child: Text(
                        numPadIndex,
                        style: TextStyle(
                          color: textColor,
                          fontSize: widget.fontSize,
                        ),
                      ),
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      ],
    );
  }

  void buttonTaped(String button) {
    var userInput = button;
    var correctAnswer = firstNumber + secondNumber;
    if (button == "Ans") {
      if (showInTextFill == correctAnswer.toString()) {
        // show correct
        setState(() {

          // firstNumber = RandomNumber(2);
          // secondNumber = RandomNumber(2);
        });

        print(firstNumber.toString() + " + " + secondNumber.toString());
        showInTextFill = "";
      } else {
        // show incorrect
        showDialog(
            context: context,
            builder: (context) {
              return Dialog_Incorrect(onTap: backQuestion);
            });
        showInTextFill = "";
      }
    } else if (button == "Del") {
      showInTextFill = "";
    } else {
      if (showInTextFill == "Answer" ||
          showInTextFill == "Ans" ||
          showInTextFill == "Del") {
        showInTextFill = "";
      }
      showInTextFill += userInput;
      userInput = "";
    }

    setState(() {
      showInTextFill;
      print(showInTextFill);
    });
  }

  void backQuestion() {
    Navigator.of(context).pop();
  }
}

int RandomNumber(int number) {
  return number = Random().nextInt(10);
}
