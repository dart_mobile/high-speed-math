import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/countdown_timer_controller.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'package:high_speed_math/Store/appmodel.dart';
import 'package:provider/provider.dart';
import 'AlertDialog/Incorrect.dart';
import 'AppBarMath.dart';
import 'Game.dart';
import 'MainGame/result_score.dart';
import 'Text.dart';

String showInTextFill = "Answer";

class BuildGameMobileVertical extends StatefulWidget {
  final double fontSize;
  final double aspectRatio;
  final double paddingSize;

  final appbarHeight;
  final appbarWidth;

  final ValueChanged incrementCounter;
  final ValueChanged randomNumber;
  final int firstNumber;
  final int secondNumber;
  final int score;

  final textFillWidth;
  final textFillHeight;

  const BuildGameMobileVertical(
      {Key? key,
      required this.fontSize,
      required this.aspectRatio,
      required this.paddingSize,
      required this.textFillWidth,
      required this.textFillHeight,
      required this.incrementCounter,
      required this.firstNumber,
      required this.secondNumber,
      required this.score,
      required this.randomNumber,
      required this.appbarHeight,
      required this.appbarWidth})
      : super(key: key);

  @override
  _BuildGameMobileVerticalState createState() =>
      _BuildGameMobileVerticalState();
}

class _BuildGameMobileVerticalState extends State<BuildGameMobileVertical> {
  final showText = "";

  var btnColor = Colors.white70;
  var textColor = Colors.white;
  var numPadIndex;

  final level = "1";

  late CountdownTimerController controller;

  int endTime = DateTime.now().millisecondsSinceEpoch + 30000;

  @override
  void initState() {
    super.initState();
    controller = CountdownTimerController(endTime: endTime);
  }

  final List<String> numPad = [
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    'Del',
    '0',
    'Ans'
  ];

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;

    final fontSize = screenHeight * 0.02;

    //number pad
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.lightBlue[700],
        toolbarHeight: widget.appbarHeight,
        title: CountdownTimer(
          textStyle: TextStyle(fontSize: widget.appbarHeight * 0.5),
          endTime: endTime,
          onEnd: () {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => result_Score(
                      score: widget.score,
                    )));
          },
        ),
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios,
            size: widget.appbarHeight * 0.5,
          ),
          //replace with our own icon data.
        ),
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 20, top: widget.appbarHeight * 0.2),
            child: Text(
              '${widget.score}',
              style: const TextStyle(color: Colors.white, fontSize: 25),
            ),
          ),
        ],
      ),
      body: Container(
        color: Colors.lightBlue.shade900,
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            children: [
              Expanded(
                flex: 3,
                child: Center(
                  child: Text(
                    "${widget.firstNumber} + ${widget.secondNumber} = ?",
                    style: TextStyle(
                        color: Colors.white, fontSize: widget.fontSize * 2.5),
                  ),
                ),
              ),
              buildText(
                fontSize: fontSize * 1.3,
              ),
              Expanded(
                flex: 1,
                child: Center(
                  child: Container(
                    width: widget.textFillWidth,
                    height: widget.textFillHeight,
                    decoration: BoxDecoration(
                      color: Colors.lightBlue.shade700,
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: Center(
                      child: Text(
                        showInTextFill,
                        style: TextStyle(
                            fontSize: fontSize, color: Colors.grey.shade400),
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 3,
                child: GridView.builder(
                  itemCount: numPad.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                    childAspectRatio: widget.aspectRatio,
                  ),
                  itemBuilder: (context, index) {
                    numPadIndex = numPad[index];
                    if (numPadIndex == "Del") {
                      btnColor = Colors.red.shade400;
                      textColor = Colors.white;
                    } else if (numPadIndex == "Ans") {
                      btnColor = Colors.green.shade400;
                      textColor = Colors.white;
                    } else {
                      btnColor = Colors.white70;
                      textColor = Colors.black;
                    }
                    return Padding(
                      padding: EdgeInsets.all(widget.paddingSize),
                      child: GestureDetector(
                        onTap: () => buttonTaped(numPad[index]),
                        child: Container(
                          decoration: BoxDecoration(
                            color: btnColor,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Center(
                            child: Text(
                              numPadIndex,
                              style: TextStyle(
                                color: textColor,
                                fontSize: widget.fontSize,
                              ),
                            ),
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void buttonTaped(String button) {
    var userInput = button;
    var correctAnswer = widget.firstNumber + widget.secondNumber;
    if (button == "Ans") {
      if (showInTextFill == correctAnswer.toString()) {
        _onPressed();
      } else {
        // show incorrect
        showDialog(
            context: context,
            builder: (context) {
              return Dialog_Incorrect(onTap: backQuestion);
            });

        showInTextFill = "";
      }
    } else if (button == "Del") {
      showInTextFill = "";
    } else {
      if (showInTextFill == "Answer" ||
          showInTextFill == "Ans" ||
          showInTextFill == "Del") {
        showInTextFill = "";
      }
      showInTextFill += userInput;
      userInput = "";
    }

    setState(() {
      showInTextFill;
      print(showInTextFill);
    });
  }

  void backQuestion() {
    Navigator.of(context).pop();
  }

  void _onPressed() {
    setState(() {
      widget.incrementCounter(null);
      widget.randomNumber(null);
      widget.score;
      showInTextFill = "";
    });
  }
}
