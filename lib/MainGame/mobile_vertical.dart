import 'package:flutter/material.dart';

import './mobile_horizontal.dart';

import '../GameUi_mobile_vertical.dart';

class MobileBodyVertical extends StatefulWidget {
  final ValueChanged incrementCounter;
  final ValueChanged randomNumber;
  final int firstNumber;
  final int secondNumber;
  final int score;

  const MobileBodyVertical(
      {Key? key,
      required this.score,
      required this.incrementCounter,
      required this.firstNumber,
      required this.secondNumber,
      required this.randomNumber})
      : super(key: key);

  @override
  State<MobileBodyVertical> createState() => _MobileBodyVerticalState();
}

class _MobileBodyVerticalState extends State<MobileBodyVertical> {
  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;

    final fontSize = screenHeight * 0.03;
    final aspectRatio = screenWidth * 0.0049;
    final paddingSize = (screenHeight / screenWidth) * 3.5;
    final textFillWidth = screenWidth * 0.90;
    final textFillHeight = screenWidth * 0.14;
    final appbarHeight = screenHeight * 0.06;
    final appbarWidth = screenHeight * 0.05;
    final textInput = screenHeight * 0.1;

    if (screenWidth > screenHeight) {
      return MobileBodyHorizontal(
        score: widget.score,
        incrementCounter: widget.incrementCounter,
        firstNumber: widget.firstNumber,
        secondNumber: widget.secondNumber,
        randomNumber: widget.randomNumber,
      );
    } else {
      return BuildGameMobileVertical(
        fontSize: fontSize,
        aspectRatio: aspectRatio,
        paddingSize: paddingSize,
        textFillWidth: textFillWidth,
        textFillHeight: textFillHeight,
        incrementCounter: widget.incrementCounter,
        firstNumber: widget.firstNumber,
        secondNumber: widget.secondNumber,
        score: widget.score,
        randomNumber: widget.randomNumber,
        appbarHeight: appbarHeight,
        appbarWidth: appbarWidth,
      );
    }
  }
}
