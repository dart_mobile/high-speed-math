class AppModel {
  String _score = '10';

  String get score => _score;

  set score(String score) {
    _score = score;
  }
}
