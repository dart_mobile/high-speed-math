import 'package:flutter/material.dart';

class buildText extends StatelessWidget {
  final fontSize;

  const buildText({Key? key, required this.fontSize}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
          padding: EdgeInsets.only(left: 30, right: 30),
          child: Text(
            "Input your answers",
            style: TextStyle(
                fontSize: fontSize,
                fontWeight: FontWeight.w400,
                color: Colors.white),
          )),
    );
  }
}
